<?php
/**
 * @file
 * GroupDocs module.
 */



/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function groupdocs_assembly_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'groupdocs_assembly_form' => array(
      'label' => t('GroupDocs assembly'),
      'description' => t('Add assembly form to your Drupal nodes.'),
      'default_widget' => 'groupdocs_assembly_guid',
      'default_formatter' => 'groupdocs_assembly_iframe',
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 *
 * This formatter just displays groupdocs iframe
 *
 * @see groupdocs_assembly_field_formatter_view()
 */
function groupdocs_assembly_field_formatter_info() {
  return array(
    'groupdocs_assembly_iframe' => array(
      'label' => t('Embedded GroupDocs assembly'),
      'field types' => array('groupdocs_assembly_form'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 *
 * Renders the ouput of an 'Embedded Groupdocs assembly'
 * formatted field within an iframe that
 * pulls in the Groupdocs assembly to display the file inline.
 */
function groupdocs_assembly_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  switch ($display['type']) {
    // This formatter outputs the field within an iframe.
    case 'groupdocs_assembly_iframe':
      foreach ($items as $delta => $item) {
        $src = $item['groupdocs_assembly_form'];

        $plugin_version = '1.0.0';
        $element[$delta]['#markup'] = "<iframe src='https://apps.groupdocs.com/assembly2/questionnaire-assembly/$src?&referer=drupal/$plugin_version' frameborder='0' width='600' height='700'></iframe>";
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 *
 * This widget type will add fancybox (jQuery plugin)
 * and link to open iframe with form for file uploadin
 *
 * @see groupdocs_assembly_field_widget_form()
 */
function groupdocs_assembly_field_widget_info() {
  return array(
    'groupdocs_assembly_guid' => array(
      'label' => t('Enter form GUID'),
      'field types' => array('groupdocs_assembly_form'),
    ),
  );
}
/**
 * Implements hook_field_widget_form().
 *
 * This hook add one text input for uploaded file guid
 * link to iframe with upload file form
 * and attached fancybox (jQuery plugin)
 */
function groupdocs_assembly_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['groupdocs_assembly_form']) ? $items[$delta]['groupdocs_assembly_form'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {

    case 'groupdocs_assembly_guid':

      $widget += array(
        '#type' => 'textfield',
        '#default_value' => $value,
        // Allow a slightly larger size that the field length to allow for some
        // configurations where all characters won't fit in input field.
        '#size' => 32,
        '#maxlength' => 255,
      );
      break;
  }

  $element['groupdocs_assembly_form'] = $widget;
  return $element;
}


/**
 * Implements hook_field_is_empty().
 *
 * hook_field_is_emtpy() is where Drupal asks us if this field is empty.
 * Return TRUE if it does not contain data, FALSE if it does. This lets
 * the form API flag an error when required fields are empty.
 */
function groupdocs_assembly_field_is_empty($item, $field) {
  return empty($item['groupdocs_assembly_form']);
}
