GroupDocs Assembly module can be used to easily collect data entered by users through online forms and then automatically incorporate that data into PDF or Word document templates. It’s a convenient method of generating standard documents where user-specific details are required to be filled in for each individual partner/customer (for instance, quotes, contracts, invoices, etc.).

To setup the automated document assembly process, simply install the GroupDocs module on your Drupal website and then follow these steps:

    Prepare a template by adding merge fields to a document you want to be assembled. Both PDF and Word document formats are accepted.
    Upload the template to your GroupDocs account.
    Design an online questionnaire that corresponds with the fields in the template. You can create questionnaires right from the GroupDocs interface.
    Embed the questionnaire into one of your Drupal web pages.
    Invite users to complete the questionnaire. For each user that completes the questionnaire, a new document will be created, containing the data entered by the user.


-- INSTALLATION --

1. Unpack the groupdocs_assembly folder and contents in the appropriate modules
   directory of your Drupal installation. This is probably sites/all/modules/
2. Enable Embedded Groupdocs Assembly in the admin modules section.
3. Click on "manage fields" under Home » Administration » Structure and
   add new field with type "GroupDocs assembly".
4. Go edit or add new node of type that you added new field to. Enter form
   GUID to the text input.

   Note: Current version of module support creation of only one field!
